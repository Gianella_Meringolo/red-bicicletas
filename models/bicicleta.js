var Bicicleta= function (id, color, modelo, ubicacion) {
    this.id= id;
    this.color= color;
    this.modelo= modelo;
    this.ubicacion= ubicacion;
}

Bicicleta.prototype.toString= function(){
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis=[];
Bicicleta.add= function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById= function(aBiciId){
    var aBici= Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error('No existe una bicicleta con el id $(aBiciId)');
}

Bicicleta.removeById =function(aBiciId){
    for(var i=0; 1< Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[1].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

/*agregamos 2 bicicletas
var a = new Bicicleta(1, 'rojo', 'urbana', [-34.893675, -56.103056]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.895822, -56.102606]);

Bicicleta.add(a);
Bicicleta.add(b);*/

module.exports = Bicicleta;