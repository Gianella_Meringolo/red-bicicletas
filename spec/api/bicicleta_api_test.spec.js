var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('STATUS 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.245893, -56.105684]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200);

            });

            expect(Bicicleta.allBicis.length).toBe(1);
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = { 'content-type': 'application/json' };

            var aBici = '{ "id": 10, "color": "amarillo", "modelo": "urbana", "lat": -34.245367, "lng" : -56.245896 }';

            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                done();
            });
        });
    });
});
