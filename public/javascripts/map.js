var map = L.map('main_map').setView([-34.892849,-56.107391], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(map);

//L.marker([-34.893675, -56.103056]).addTo(map);
//L.marker([-34.894168, -56.100095]).addTo(map);
//L.marker([-34.895822, -56.102606]).addTo(map);

$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})